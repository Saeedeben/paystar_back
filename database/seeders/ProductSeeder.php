<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'name'       => "کاپشن",
                'count'      => 10,
                'price'      => 10000,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name'  => "کیف",
                'count' => 10,
                'price' => 10000,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name'  => "شلوار جین",
                'count' => 10,
                'price' => 10000,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name'  => "کلاه",
                'count' => 10,
                'price' => 10000,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ];
        \DB::table('products')->insert($products);
    }
}
