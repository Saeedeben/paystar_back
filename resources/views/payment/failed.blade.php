<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Success</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            text-align: center;
            background-color: #e1e1e1;
        }

        .success {
            width: 800px;
            height: 500px;
            margin-top: 100px;
            background-color: white;
        }
    </style>
</head>
<body>
<div class="success">

    <h1>نا‌موفق</h1>
    <p>transaction_id: {{$transactionId}}</p>
    <p>خطا در انجام تراکنش. در صورت کسر مبلغ از حساب شما تا ۷۲ ساعت دیگر به حساب شما برخواهد گشت.</p>
    <p>این اتفاق ممکن است به دلیل عدم تطابق شماره کارت اعلام شده با شماره کارت پرداختی رخ داده باشد.</p>
    <p>پیام تایید پرداخت: {{$message}}</p>

    <form action="http://94.101.180.121/" method="GET">
        <button class="btn btn-primary" type="submit">بازگشت به سایت</button>
    </form>
</div>
</body>
</html>
