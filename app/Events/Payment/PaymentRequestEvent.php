<?php

namespace App\Events\Payment;

use App\Models\Order;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PaymentRequestEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public string $ref_num;

    public string $card_number;

    public Order $order;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($order, $ref_num, $card_number)
    {
        $this->order       = $order;
        $this->ref_num     = $ref_num;
        $this->card_number = $card_number;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
