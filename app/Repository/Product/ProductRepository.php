<?php

namespace App\Repository\Product;

use App\Http\Resources\Product\IndexProductResource;
use App\Models\Product;

class ProductRepository
{
    public function index() :\Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        $products = Product::all();

        return IndexProductResource::collection($products);
    }
}
