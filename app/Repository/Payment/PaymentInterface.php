<?php

namespace App\Repository\Payment;

interface PaymentInterface
{
    public function payment($order, $card_number);

    public function verify($data);
}
