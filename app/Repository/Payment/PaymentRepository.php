<?php

namespace App\Repository\Payment;

use App\Models\Payment;
use App\Repository\Http\HttpRepository;

class PaymentRepository
{
    public function prepareData($order, $url, $card_number)
    {
        $conf        = config('gateway.paystar');
        $orderId     = $order->id;
        $conf['url'] .= $url;
        $amount      = $order->invoice->amount;

        return [$this->headers($conf), $this->body($amount, $orderId, $conf, $card_number), $conf];
    }

    private function body($amount, $orderId, $conf, $card_number)
    {
        return [
            "amount"          => $amount,
            "order_id"        => $orderId,
            "callback"        => $conf['callback'],
            "sign"            => $this->generateSign($amount, $orderId, $conf),
            "card_number"     => $card_number,
            "callback_method" => 1,
        ];
    }

    private function generateSign($amount, $orderId, $conf) :bool|string
    {
        return hash_hmac('sha512', "$amount#{$orderId}#{$conf['callback']}", $conf['hash_key']);
    }

    private function headers($conf)
    {
        return [
            'Content-Type'  => 'application/json',
            'Authorization' => "Bearer {$conf['gateway_id']}",
        ];
    }

    public function checkCardNumber($data) :bool
    {
        $payment = $this->get($data);

        $oldCardNumberFirst = substr($payment->card_number, 0, 6);
        $oldCardNumberLast  = substr($payment->card_number, 0, 4);
        $newCardNumberFirst = substr($data->card_number, 0, 6);
        $newCardNumberLast  = substr($data->card_number, 0, 4);

        return ($oldCardNumberFirst === $newCardNumberFirst && $oldCardNumberLast === $newCardNumberLast);
    }


    public function verifyWithPaystar($conf, $data) :\GuzzleHttp\Promise\PromiseInterface|\Illuminate\Http\Client\Response
    {
        $url     = $conf['url'] . 'verify';
        $headers = $this->headers($conf);
        $body    = $this->verifyRequestBody($data);

        return HttpRepository::post($headers, $body, $url);
    }

    public function verifyRequestBody($data)
    {
        /** @var Payment $payment */
        $payment = $this->get($data);

        return [
            'ref_num' => $payment->ref_num,
            'amount'  => $payment->order->invoice->amount,
        ];
    }

    public function get($data) :object
    {
        return Payment::with('order')
            ->where('order_id', $data->order_id)
            ->where('ref_num', $data->ref_num)
            ->first();
    }


}
