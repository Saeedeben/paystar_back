<?php

namespace App\Repository\Payment;

use App\Events\Payment\PaymentRequestEvent;
use App\Events\Payment\VerifyPaymentEvent;
use App\Repository\Http\HttpRepository;
use Illuminate\Support\Facades\Redirect;

class PaystarGateway implements PaymentInterface
{

    private PaymentRepository $paymentRepository;
    private array             $conf;

    public function __construct()
    {
        $this->paymentRepository = new PaymentRepository();
        $this->conf              = config('gateway.paystar');
    }

    public function payment($order, $card_number) :string
    {
        $order->load('invoice');
        $url = 'create';

        [$headers, $body, $conf] = $this->paymentRepository->prepareData($order, $url, $card_number);

        $response = HttpRepository::post($headers, $body, $conf['url']);
        $response = $this->decodeResponse($response);


        event(new PaymentRequestEvent($order, $response['data']['ref_num'], $card_number));

        return $this->redirect($response['data']['token']);
    }

    public function decodeResponse($json)
    {
        return json_decode($json, true);
    }

    public function redirect($token)
    {
        return $this->conf['url'] . 'payment?token=' . $token;
    }

    public function verify($data) :\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Auth\Access\Response|bool|\Illuminate\Contracts\Foundation\Application
    {
        $status = (int)$data->status > 0;

        if (!$this->paymentRepository->checkCardNumber($data)) {
            $status = false;
        }

        event(new VerifyPaymentEvent($data, $status));

        if ($status) {
            $verify = $this->paymentRepository->verifyWithPaystar($this->conf, $data);
            $verify = $this->decodeResponse($verify);

            if ($verify['status'] != 1) {
                return view('payment/failed', ['transactionId' => $data->transaction_id, 'message' => $verify['message']]);
            }

            return view('payment/success', [
                'transactionId' => $data->transaction_id,
                'tracking_code' => $data->tracking_code,
                'card_number'   => $data->card_number,
                'amount'        => $verify['data']['price'],
                'message'       => $verify['message'],
            ]);
        } else {
            return view('payment/failed', ['transactionId' => $data->transaction_id, 'message' => 'عملیات ناموق']);
        }
    }

}
