<?php

namespace App\Repository\Order;

use App\Models\Invoice;

class InvoiceRepository
{
    public function store($sumPrice) :Invoice
    {
        $invoice              = new Invoice();
        $invoice->amount      = $sumPrice;
        $invoice->description = 'پرداخت درگاه paystar';
        $invoice->save();

        return $invoice;
    }
}
