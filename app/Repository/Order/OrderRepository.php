<?php

namespace App\Repository\Order;

use App\Models\Order;
use App\Models\Product;

class OrderRepository
{
    public function store($request, $invoice) :Order
    {
        $order         = new Order();
        $order->status = Order::STATUS_INVOICED;
        $order->customer()->associate(1);
        $order->invoice()->associate($invoice->id);
        $order->save();

        foreach ($request->products as $product) {
            $insert[$product['id']] = ['count' => $product['count']];
            $order->products()->sync($insert);
        }

        return $order;
    }

    public function checkOrderAvailability($data) :array
    {
        $check            = true;
        $notEnoughProduct = 0;
        $pIds             = collect($data->products)->pluck('id')->toArray();

        $products = Product::query()
            ->whereIn('id', $pIds)
            ->get();

        foreach ($data->products as $datum) {
            $product = $products->where('id', $datum['id'])->first();
            if ($product->count < (int)$datum['count']) {
                $check            = false;
                $notEnoughProduct = $product->name;
            }
        }

        return [$check, $notEnoughProduct];
    }

    public function show()
    {
        return Order::query()
            ->where('customer_id', 1)
            ->where('status', Order::STATUS_INVOICED)
            ->first();
    }

    public function reject()
    {
        Order::where('customer_id', 1)
            ->where('status', Order::STATUS_INVOICED)
            ->update([
                'status' => Order::STATUS_CANCELED,
            ]);
    }

    public function checkPastOrder()
    {
        return Order::where('customer_id', 1)
            ->where('status', Order::STATUS_INVOICED)
            ->first();
    }
}
