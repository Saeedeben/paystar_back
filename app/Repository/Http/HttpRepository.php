<?php

namespace App\Repository\Http;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class HttpRepository
{
    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function post($headers, $body, $url)
    {
        return Http::withHeaders($headers)
            ->post($url, $body);
    }
}
