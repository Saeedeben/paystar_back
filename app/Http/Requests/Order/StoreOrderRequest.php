<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @property integer $sum_price
 * @property array   $products
 */
class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'sum_price'        => 'required|integer',
            'products'         => 'array',
            'products.*.id'    => [
                'required',
                Rule::exists('products', 'id'),
            ],
            'products.*.count' => 'required|integer',
        ];
    }
}
