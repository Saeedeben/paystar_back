<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\Product\IndexProductResource;
use App\Models\Order;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Order
 */
class ShowOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'customer'    => $this->customer->name,
            'amount'      => $this->invoice->amount,
            'description' => $this->invoice->description,
            'products'    => ShowOrderProductResource::collection($this->products)
        ];
    }
}
