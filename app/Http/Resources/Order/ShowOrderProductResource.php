<?php

namespace App\Http\Resources\Order;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Product
 */
class ShowOrderProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name'      => $this->name,
            'price'     => $this->price,
            'count'     => $this->pivot->count,
            'sum_price' => $this->price * $this->pivot->count,
        ];
    }
}
