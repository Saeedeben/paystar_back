<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\IndexProductRequest;
use App\Http\Resources\Product\IndexProductResource;
use App\Models\Product;
use App\Repository\Product\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private ProductRepository $productRepo;


    public function __construct()
    {
        $this->productRepo = new ProductRepository();
    }

    /**
     * @param IndexProductRequest $request
     *
     * @return array
     */
    public function index(IndexProductRequest $request) :array
    {
        $products = $this->productRepo->index();

        return $this->response(true,200,'',$products);
    }
}
