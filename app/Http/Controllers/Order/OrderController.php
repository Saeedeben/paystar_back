<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Http\Requests\Order\StoreOrderRequest;
use App\Http\Resources\Order\ShowOrderResource;
use App\Models\Order;
use App\Repository\Order\InvoiceRepository;
use App\Repository\Order\OrderRepository;

class OrderController extends Controller
{
    private InvoiceRepository $invoiceRepo;
    private OrderRepository   $orderRepo;

    public function __construct()
    {
        $this->invoiceRepo = new InvoiceRepository();
        $this->orderRepo   = new OrderRepository();
    }

    /**
     * @param StoreOrderRequest $request
     *
     * @return array
     */
    public function store(StoreOrderRequest $request) :array
    {
//        try {
            /** @var Order $order */
            $order = $this->orderRepo->checkPastOrder();

            if ($order) {
                return $this->response(false, 491, trans('responses.order.order_exist'), ['order_id' => $order->id]);
            }

            [$check, $notEnoughProduct] = $this->orderRepo->checkOrderAvailability($request);
            if (!$check) {
                return $this->response(false, 492, trans('responses.order.not_enough_product', ['attribute' => $notEnoughProduct]));
            }


            $invoice = $this->invoiceRepo->store($request->sum_price);

            $this->orderRepo->store($request, $invoice);

            return $this->response(true, 200, trans('responses.order.store'));

//        } catch (\Exception $error) {
//            \Log::info($error);
//
//            return $this->response(false, 497, $error->getMessage());
//        }
    }

    /**
     * @return array
     */
    public function show() :array
    {
        $order = $this->orderRepo->show();

        if (!$order) {
            return $this->response(false, 493, trans('responses.order.not_found'));
        }

        return $this->response(true, 200, '', new ShowOrderResource($order));
    }

    /**
     * @return array
     */
    public function reject() :array
    {
        $this->orderRepo->reject();

        return $this->response(true, 200, trans('responses.order.reject'));
    }

}
