<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Http\Requests\Payment\PaymentRequest;
use App\Models\Order;
use App\Models\Product;
use App\Repository\Payment\PaystarGateway;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    private PaystarGateway $paystarGateway;

    public function __construct()
    {
        $this->paystarGateway = new PaystarGateway();
    }

    public function pay(Order $order, PaymentRequest $request) :string
    {
        return $this->paystarGateway->payment($order, $request->card_number);
    }

    public function verify(Request $req) :\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Auth\Access\Response|bool|\Illuminate\Contracts\Foundation\Application
    {
        return $this->paystarGateway->verify($req);
    }
}
