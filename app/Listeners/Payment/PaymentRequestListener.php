<?php

namespace App\Listeners\Payment;

use App\Events\Payment\PaymentRequestEvent;
use App\Models\Payment;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PaymentRequestListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\Payment\PaymentRequestEvent $event
     *
     * @return void
     */
    public function handle(PaymentRequestEvent $event)
    {
        $payment              = new Payment();
        $payment->ref_num     = $event->ref_num;
        $payment->card_number = $event->card_number;
        $payment->order()->associate($event->order->id);
        $payment->save();
    }
}
