<?php

namespace App\Listeners\Payment;

use App\Events\Payment\VerifyPaymentEvent;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Product;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class VerifyPaymentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\Payment\VerifyPaymentEvent $event
     *
     * @return void
     */
    public function handle(VerifyPaymentEvent $event)
    {
        $status      = $event->status ? Payment::STATUS_SUCCESS : Payment::STATUS_FAILED;
        $orderStatus = $event->status ? Order::STATUS_PAYED : Order::STATUS_CANCELED;

        Payment::where('order_id', $event->data->order_id)
            ->where('ref_num', $event->data->ref_num)
            ->update([
                'transaction_id' => $event->data->transaction_id,
                'tracking_code'  => $event->data->tracking_code,
                'status'         => $status,
            ]);

        /** @var Order $order */
        $order = Order::with('products')
            ->where('id', $event->data->order_id)
            ->first();

        $order->update([
            'status' => $orderStatus,
        ]);

        if ($event->status) {
            $orderProducts = $order->products->pluck('id')->toArray();

            $products = Product::whereIn('id', $orderProducts)
                ->get()
                ->toArray();

            foreach ($products as $key => $product) {
                $orderCount              = $order->products()->where('product_id', $product['id'])->first()->pivot->count;
                $products[$key]['count'] = $product['count'] - $orderCount;
                unset($products[$key]['created_at']);
                unset($products[$key]['updated_at']);
                unset($products[$key]['deleted_at']);
            }

            Product::upsert($products, ['id'], ['count']);
        }

    }
}
