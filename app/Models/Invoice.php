<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int     $id
 *
 * @property integer $amount
 * @property string  $description
 *
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 * @property Carbon  $deleted_at
 * -------------------------------------- Relations
 *
 * -------------------------------------- Attributes
 *
 */
class Invoice extends Model
{
    use HasFactory;

    protected $table = 'invoices';

    protected $fillable = [
        'amount' , 'description'
    ];

    // Relations ------------------------------------------------------------------------
    public function order() :\Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
    // Attributes ------------------------------------------------------------------------
    // Methods ------------------------------------------------------------------------
    // Scopes ------------------------------------------------------------------------
}
