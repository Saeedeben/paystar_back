<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int    $id
 *
 * @property string $name
 * @property string $count
 * @property string $price
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * -------------------------------------- Relations
 *
 * -------------------------------------- Attributes
 *
 */
class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'products';

    protected $fillable = [
        'name', 'count', 'price',
    ];

    // Relations ------------------------------------------------------------------------
    // Attributes ------------------------------------------------------------------------
    // Methods ------------------------------------------------------------------------
    // Scopes ------------------------------------------------------------------------
}
