<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 *
 * @property string $ref_num
 * @property string $transaction_id
 * @property string $card_number
 * @property int    $invoice_id
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * -------------------------------------- Relations
 * @property Order  $order
 * -------------------------------------- Attributes
 *
 */
class Payment extends Model
{
    use HasFactory;

    protected $table = 'payments';

    protected $fillable = [
        'ref_num', 'transaction_id',
    ];

    const STATUS_SUCCESS = 'success';
    const STATUS_FAILED  = 'failed';
    const STATUS_PENDING = 'pending';
    const STATUSES       = [
        self::STATUS_SUCCESS,
        self::STATUS_FAILED,
        self::STATUS_PENDING,
    ];

    // Relations ------------------------------------------------------------------------
    public function order() :\Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
    // Attributes ------------------------------------------------------------------------
    // Methods ------------------------------------------------------------------------
    // Scopes ------------------------------------------------------------------------
}
