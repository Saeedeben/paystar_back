<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int      $id
 *
 * @property string   $status
 * @property string   $card_number
 *
 * @property integer  $customer_id
 *
 * @property Carbon   $created_at
 * @property Carbon   $updated_at
 * @property Carbon   $deleted_at
 * -------------------------------------- Relations
 * @property Product  $products
 * @property Customer $customer
 * @property Invoice  $invoice
 * -------------------------------------- Attributes
 *
 */
class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'orders';

    protected $fillable = [
        'price', 'status', 'card_number',
    ];

    const STATUS_PAYED    = 'payed';
    const STATUS_CANCELED   = 'canceled';
    const STATUS_INVOICED = 'invoiced';
    const STATUSES        = [
        self::STATUS_PAYED,
        self::STATUS_CANCELED,
        self::STATUS_INVOICED,
    ];

    // Relations ------------------------------------------------------------------------
    public function products() :\Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'order_products', 'order_id', 'product_id', 'id')
            ->withPivot(['count']);
    }

    public function customer() :\Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function invoice() :\Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Invoice::class);
    }
    // Attributes ------------------------------------------------------------------------
    // Methods ------------------------------------------------------------------------
    // Scopes ------------------------------------------------------------------------
}
