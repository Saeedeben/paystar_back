<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int    $id
 *
 * @property string $name
 * @property string $mobile
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * -------------------------------------- Relations
 *
 * -------------------------------------- Attributes
 *
 */
class Customer extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'customers';

    protected $fillable=[
        'name', 'mobile'
    ];

    // Relations ------------------------------------------------------------------------
    // Attributes ------------------------------------------------------------------------
    // Methods ------------------------------------------------------------------------
    // Scopes ------------------------------------------------------------------------

}
