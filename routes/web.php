<?php

use App\Http\Controllers\Order\OrderController;
use App\Http\Controllers\Payment\PaymentController;
use App\Http\Controllers\Product\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'paystar'], function () {
    // products ------------------------------------------------------------------------
    Route::get('products', [ProductController::class, 'index']);

    // order ------------------------------------------------------------------------
    Route::post('order', [OrderController::class, 'store']);
    Route::get('order', [OrderController::class, 'show']);
    Route::get('order/reject', [OrderController::class, 'reject']);

    // payment ------------------------------------------------------------------------
    Route::post('payment/order/{order}', [PaymentController::class, 'pay']);
    Route::get('payment/verify', [PaymentController::class, 'verify']);
});
